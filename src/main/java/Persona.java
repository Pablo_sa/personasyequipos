import java.util.Collections;
import java.util.Comparator;

public class Persona {
    private Integer dni;
    private Integer equipo;

    public Persona() { }

    public Persona(Integer dni, Integer equipo) {
        this.dni = dni;
        this.equipo = equipo;
    }

    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public Integer getEquipo() {
        return equipo;
    }

    public void setEquipo(Integer equipo) {
        this.equipo = equipo;
    }

    @Override
    public String toString() {
        return "Persona [dni=" + dni + ", equipo=" + equipo + "]";
    }

    /*
    public int compare(Object o1, Object o2) {
        Persona persona1 = (Persona)o1;
        Persona persona2 = (Persona)o2;
        return compare(persona1.getEquipo(), persona2.getEquipo());
    }*/
}
