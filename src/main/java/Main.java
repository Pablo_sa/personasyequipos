import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Persona persona1 = new Persona(1, 1);
        Persona persona2 = new Persona(2, 1);
        Persona persona3 = new Persona(3, 1);
        Persona persona4 = new Persona(4, 2);
        Persona persona5 = new Persona(5, 2);
        Persona persona6 = new Persona(6, 2);

        ArrayList<Persona> personas = new ArrayList<Persona>();

        personas.add(persona1);
        personas.add(persona2);
        personas.add(persona3);
        personas.add(persona4);
        personas.add(persona5);
        personas.add(persona6);
        System.out.println(personas);

        Persona persona7 = new Persona(7, 1);
        Persona persona8 = new Persona(8, 2);

        ArrayList<Persona> otrasPersonas = new ArrayList<Persona>();
        otrasPersonas.add(persona7);
        otrasPersonas.add(persona8);
        System.out.println(otrasPersonas);

        List<Persona> personasListaConcat = Stream.concat(personas.stream(), otrasPersonas.stream()).collect(Collectors.toList());
        System.out.println(personasListaConcat);

        personasListaConcat.stream().sorted(Comparator.comparing(Persona::getEquipo)).forEach(System.out::println);

        Collections.sort(personasListaConcat, new Comparator<Persona>() {
            @Override
            public int compare(Persona p1, Persona p2) {
                return new Integer(p1.getEquipo()).compareTo(new Integer(p2.getEquipo()));
            }
        });

        System.out.println(personasListaConcat);
    }
}

